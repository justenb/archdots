[colors]
background = #5a000000
foreground = #eee
background-alt = #444
foreground-alt = #555
primary = #ffb52a
secondary = #e60053
alert = #bd2c40

[settings]
screenchange-reload = true
pseudo-transparency = false

[bar/ragbar]
monitor = ${env:MONITOR:}

width = 100%
height = 35
fixed-center = false

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3

padding-left = 2
padding-right = 2

module-margin-left = 1
module-margin-right = 2

font-0 = fixed:pixelsize=10;1
font-1 = unifont:fontformat=truetype:size=8:antialias=false;0
font-2 = siji:pixelsize=10;1
font-3 = FontAwesome 5 Free:size=11:style=Solid;2

modules-left = bspwm
; modules-center = xwindow
modules-right = vpn network pulseaudio battery date

tray-position = right
tray-padding = 2

cursor-click = pointer
cursor-scroll = ns-resize

[global/wm]
margin-top = 5
margin-bottom = 5

[module/xwindow]
type = internal/xwindow
format = <label>
format-padding = 4
label = %title%
label-maxlen = 50

[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-background = ${colors.background-alt}
label-focused-underline= ${colors.foreground}
label-focused-padding = 2

label-occupied = %index%
label-occupied-padding = 2

label-urgent = %index%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

label-empty = %index%
label-empty-foreground = ${colors.foreground-alt}
label-empty-padding = 2

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <ramp-volume> <label-volume>
label-volume =  %percentage%%
label-volume-foreground = ${root.foreground}

ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 

label-muted =  mute
label-muted-foreground = #666

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator-font = 2
bar-volume-fill-font = 2
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}

[module/vpn]
type = custom/script
exec = ~/bin/nordvpn-dmenu -s 
interval = 10
click-left = ~/bin/nordvpn-dmenu


[module/network]
type = internal/network
interface = wlp82s0

; sleep time betwen update
interval = 4.0


format-connected =  <label-connected>
format-disconnected = 

label-connected = %essid%
label-connected-foreground = ${colors.foreground}

label-disconnected = not connected
label-disconnected-foreground = ${colors.foreground}

[module/battery]
type = internal/battery
battery = BAT0
adapter = AC
full-at = 100

format-full =  100%
format-charging = <ramp-capacity> <label-charging>
format-discharging = <ramp-capacity> <label-discharging>

format-full-prefix = " "
format-full-prefix-foreground = ${colors.foreground-alt}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
ramp-capacity-foreground = ${colors.foreground}

[module/date]
type = internal/date
interval = 5

date = %m-%d
date-alt = " %Y-%m-%d"

time = %H:%M
time-alt = %H:%M:%S

format-prefix = 
format-prefix-foreground = ${colors.foreground}

label = %time% %date%

; vim:ft=dosini
